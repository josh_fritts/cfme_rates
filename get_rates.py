#!/usr/bin/python

import requests
import simplejson as json
from decimal import Decimal
import re

CFME_HOST = "https://cloudforms-beta.dev.scholastic.tech%s"
CREDS = ("admin", "smartvm")
HEADERS = {"Accept": "application/json"}
CH_API_KEY = "5335f5e3-92b5-42a9-ae8e-1b001428e8e8"
PROVIDERS = {}
INSTANCE_TYPES = {}

print "This could take QUITE a while due to utilizing poor APIs..."

def update_providers(pid):
    if pid not in PROVIDERS:
        uri = "/api/providers/%s" % pid
        prov = requests.get(CFME_HOST % uri, headers=HEADERS, auth=CREDS, verify=False)
        PROVIDERS[pid] = {"name": prov.json()["name"], "nodes": [], "price": Decimal(0), "cores": 0, "memory": 0}

r = requests.get(CFME_HOST % ("/api/container_nodes"), headers=HEADERS, auth=CREDS, verify=False)
for i in r.json()["resources"]:
    node = requests.get(i["href"], headers=HEADERS, auth=CREDS, verify=False)
    update_providers(node.json()["ems_id"])
    PROVIDERS[node.json()["ems_id"]]["nodes"].append(node.json()["identity_infra"].split("/")[4])

for cluster in PROVIDERS:
    for node in PROVIDERS[cluster]["nodes"]:
        uri = """https://chapi.cloudhealthtech.com/api/search.json?api_key=%s&name=AwsInstance&query=instance_id='%s'&include=instance_type""" % (CH_API_KEY, node)
        r = requests.get(uri, headers=HEADERS)
        try:
            hourly = r.json()[0]["hourly_cost"].lstrip("$")
        except:
            hourly = 0
        PROVIDERS[cluster]["price"] += Decimal(hourly)
        if "openshift-role: app" not in  r.json()[0]["tags"]:
            continue
        if r.json()[0]["instance_type"]["api_name"] not in INSTANCE_TYPES:
            uri = """https://chapi.cloudhealthtech.com/api/search.json?api_key=%s&name=AwsInstanceType&api_version=2&per_page=1&page=1&query=api_name='%s'""" % (CH_API_KEY, r.json()[0]["instance_type"]["api_name"])
            it = requests.get(uri, headers=HEADERS)
            INSTANCE_TYPES[r.json()[0]["instance_type"]["api_name"]] = {"cores": it.json()[0]["virtual_cores"], "memory": it.json()[0]["memory"]}
        PROVIDERS[cluster]["cores"] += INSTANCE_TYPES[r.json()[0]["instance_type"]["api_name"]]["cores"]
        PROVIDERS[cluster]["memory"] += INSTANCE_TYPES[r.json()[0]["instance_type"]["api_name"]]["memory"]
    PROVIDERS[cluster]["mem_per_hour_price"] = Decimal(PROVIDERS[cluster]["price"]) / Decimal(2) / Decimal(PROVIDERS[cluster]["memory"])
    PROVIDERS[cluster]["cpu_per_hour_price"] = Decimal(PROVIDERS[cluster]["price"]) / Decimal(2) / Decimal(PROVIDERS[cluster]["cores"])

print json.dumps(PROVIDERS, indent=4, default=str)
